//
//  PingDataReceiptImageStitcher.h
//  PingDataSDK
//
//  Created by Anuraag Sridhar on 26/4/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PingDataReceiptImageStitcher : NSObject

- (UIImage*) stitchImagesFromFilepaths:(NSArray*) imagePaths;
- (UIImage*) stitchImages:(NSArray*) images;

@end
